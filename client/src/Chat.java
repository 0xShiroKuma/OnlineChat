package OnlineChatClient;

import java.util.ArrayList;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;

public class Chat extends JFrame implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int width, height;

	private Account account;

	private Channel currentChannel;

	protected ArrayList<Channel> channels;

	private String[] channelMessages;
	
	private JLabel user, chat; 
	private JButton send;
	private JTextArea message;
	private JList channelsList;
	private DefaultListModel list;
	private JScrollPane channelsListScroller;
	private JMenuBar menuBar;
	private JMenu menu1;
	private JMenuItem menuItemConnectChannel, menuItemCreateChannel;
	
	public Chat(String id) {
		
		super("OnlineChat chat client");
		
		width = 900;
		height = 600;
		setSize(width, height);

		//get the current account
		this.account = Utilities.getAccountById(id);
		
		user = new JLabel(this.account.getUsername());

		chat = new JLabel("chat");
		message = new JTextArea();

		channelMessages = {};

		//get all user's channels
		channels = Utilities.getAccountConnectedChannels(id);
		//show only their name in the JList
		String[] data = {};
		for(int i=0;i<channels.size();i++){
			data.add(channels.get(i).getName());
		}
		JList<String> channelsList = new JList<String>(data);

		channelsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		channelsList.setLayoutOrientation(JList.VERTICAL);
		channelsListScroller = new JScrollPane(channelsList);
		send = new JButton("Send");

		//menuBar
		menuBar = new JMenuBar();
		menu1 = new JMenu("Channels");
		menuItemConnectChannel = new JMenuItem("Connect to a channel");
		menuItemConnectChannel.addActionListener(this);
		menu1.add(menuItemConnectChannel);
		menuItemCreateChannel = new JMenuItem("Create a channel");
		menuItemCreateChannel.addActionListener(this);
		menu1.add(menuItemCreateChannel);
		menuBar.add(menu1);
		setJMenuBar(menuBar);
		
		//Constraints
		GridBagConstraints c = new GridBagConstraints();
		
		JPanel mainContainer = new JPanel();
		mainContainer.setLayout(new GridBagLayout());
		
		//
		JPanel channelPan = new JPanel();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 0.25;
		c.weighty = 0.75;
		channelsList.addActionListener(this);
		channelsListScroller.setPreferredSize(new Dimension((int)(0.25 * width), (int)(0.75 * height)));
		channelPan.add(channelsListScroller);
		mainContainer.add(channelPan, c);
		
		//
		JPanel userPan = new JPanel();
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0.25;
		c.weighty = 0.25;
		userPan.add(user);
		mainContainer.add(userPan, c);
		
		//
		JPanel chatPan = new JPanel();
		c.gridx = 1;
		c.gridy = 0;
		c.weightx = 0.75;
		c.weighty = 0.75;
		chatPan.add(chat);
		mainContainer.add(chatPan, c);
		
		//
		JPanel messagePan = new JPanel();

		message.setPreferredSize(new Dimension((int)(0.75 * width * 0.8), (int)(0.25 * height)));
		send.setPreferredSize(new Dimension((int)(0.75 * width * 0.2), (int)(0.25 * height)));
		
		messagePan.add(message);
		messagePan.add(send);
		send.addActionListener(this);
		
		c.gridx = 1;
		c.gridy = 1;
		c.weightx = 0.75;
		c.weighty = 0.25;
		mainContainer.add(messagePan, c);

		
		setContentPane(mainContainer);
		setLocationRelativeTo(null);//center the frame on screen
        setVisible(true);//set frame visible
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//close the thread when frame is closed
		
	}
	
	
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();

		if(src == channelsList){
			currentChannel = channelsList.getSelectedValue();
		}
		
		if(src == menuItemConnectChannel){
			new FrameConnectChannel(account);
		}

		if(src == menuItemCreateChannel){
			new FrameCreateChannel(account);
		}

		if(src == send) {
			messages = Utilities.getChannelMessages(currentChannel);
			chat.setText(messages.toString());
			Utilities.postMessage(account, currentChannel, message.getText());
		}
		
	}
	
}