package OnlineChatClient;
import org.json.*;
import java.io.*;
import java.util.ArrayList;


class HandlerJsonResponse{
  private String requestData;
  private JSONObject jsonRequest;
  private int response_code;
  HandlerJsonResponse(String requestData){
    this.requestData = requestData;
    System.out.println("11");
    try{
      System.out.println("22");
      jsonRequest = new JSONObject(requestData);
      System.out.println("33");
    }catch(Exception e){
      //do somethings
    }
  }

  public int DetectResponseCode()
  {
    try{
      this.response_code = this.jsonRequest.getJSONObject("header").getInt("response_code");
    }
    catch(Exception e){
      return 1;
    }
    return 0;
  }

  public int interpretResponse()
  {
    switch (this.response_code) {
      case 1: return success();

      case 2: return error();

      default: return 2;
    }
  }

  public int success()
  {
    new popup_response(1);
    return 0;
  }

  public int error()
  {
    String error_msg;
    int error_code = 0;
    try{
      error_code = this.jsonRequest.getJSONObject("error").getInt("error_code");
    }catch(Exception e)
    {
      System.out.println("Error when reading error code");
    }

    switch (error_code){
      case 1: new popup_response(2, "bad request header code");
              return 0;
      case 2: new popup_response(2, "request header code doesn't exist");
              return 0;
      case 3: new popup_response(2, "bad username format");
              return 0;
      case 4: new popup_response(2, "bad passwd format");
              return 0;
      case 5: new popup_response(2, "username exits");
              return 0;
      default: new popup_response(2, "unknown error");
    }
    return 0;
  }
}
