package OnlineChatClient;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class popup_response extends JFrame{

  private JPanel mainFrame;
  private JPanel logo_response_area, message_area;
  private JLabel logo_response, message;
  private String error_msg;
  private int response_code;
  popup_response(int response_code){
    this.response_code = response_code;
    setTitle("Server response");
    setPreferredSize(new Dimension(400, 160));
    setResizable(false);
    mainFrame = new JPanel();
    setContentPane(mainFrame);
    mainFrame.setLayout(new GridBagLayout());
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.fill = GridBagConstraints.BOTH;
    gbc.insets = new Insets(5, 5, 5, 5);
    logo_response_area = new JPanel();
    message_area = new JPanel();
    logo_response = new JLabel(new ImageIcon("../src/resources/img/checked.png"));
    message = new JLabel("success");

    gbc.gridx = 0;
    gbc.gridy = 0;
    mainFrame.add(logo_response_area, gbc);
    gbc.gridx = 1;
    gbc.gridy = 0;
    mainFrame.add(message_area, gbc);

    logo_response_area.add(logo_response);
    message_area.add(message);



    setLocationRelativeTo(null);
    setVisible(true);
    pack();
  }

  popup_response(int response_code, String error_msg){
    this.response_code = response_code;
    this.error_msg = error_msg;
    setTitle("Error");
    setPreferredSize(new Dimension(400, 160));
    setResizable(false);
    mainFrame = new JPanel();
    setContentPane(mainFrame);
    mainFrame.setLayout(new GridBagLayout());
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.fill = GridBagConstraints.BOTH;
    gbc.insets = new Insets(5, 5, 5, 5);
    logo_response_area = new JPanel();
    message_area = new JPanel();
    logo_response = new JLabel(new ImageIcon("../src/resources/img/error.png"));
    message = new JLabel(error_msg);

    gbc.gridx = 0;
    gbc.gridy = 0;
    mainFrame.add(logo_response_area, gbc);
    gbc.gridx = 1;
    gbc.gridy = 0;
    mainFrame.add(message_area, gbc);

    logo_response_area.add(logo_response);
    message_area.add(message);



    setLocationRelativeTo(null);
    setVisible(true);
    pack();
  }
}
