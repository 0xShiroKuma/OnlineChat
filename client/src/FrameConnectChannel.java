package OnlineChatClient;

//import OnlineChatServer.Utilities;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;

public class FrameConnectChannel extends JFrame implements ActionListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private Account account;

    private JPanel mainContainer;

    private JLabel channelIdLabel, channelPasswdLabel, message;

    private JTextField channelId;
    private JPasswordField channelPasswd;

    private JButton connect;

    public FrameConnectChannel(Account acc) {
        super("Connect to a channel");

        account = acc;

        channelIdLabel = new JLabel("Channel ID : ");
        channelId = new JTextField(20);
        channelPasswdLabel = new JLabel("Channel password : ");
        channelPasswd = new JPasswordField(20);
        message = new JLabel("");
        connect = new JButton("Connect");

        //mainContainer
        mainContainer = new JPanel();
        mainContainer.add(channelIdLabel);
        mainContainer.add(channelId);
        mainContainer.add(channelPasswdLabel);
        mainContainer.add(channelPasswd);
        mainContainer.add(message);
        mainContainer.add(connect);
        connect.addActionListener(this);

        setContentPane(mainContainer);
		setLocationRelativeTo(null);//center the frame on screen
        setVisible(true);//set frame visible
        pack();
    }

    public void actionPerformed(ActionEvent e){

        Object src = e.getSource();
     
        if(src == connect) {

            String channel = channelId.getText();
            String passwd = String.valueOf(channelPasswd.getPassword());

            //test if creds are good
            boolean connected = Utilities.connectToChannel(acc, Utilities.getChannelsById(channel), passwd);
            if(!connected){
                System.out.println("Can't connect");
            } else {
                System.out.println("Connected");
                //super.channels.add(Utilities.getChannelsById(channel));
                this.dispose();
            }

        }

    }

}