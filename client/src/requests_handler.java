package OnlineChatClient;

import java.nio.file.Files;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;
import java.io.DataOutputStream;

public class requests_handler{
  String serverAddress;
  int serverPort;
  DataOutputStream output;
  BufferedReader input;
  Socket so;

  requests_handler(String serverAddress, int serverPort){
    this.serverAddress = serverAddress;
    this.serverPort = serverPort;
    try{
      so = new Socket(this.serverAddress, this.serverPort);
      input = new BufferedReader(new InputStreamReader(so.getInputStream()));
      output = new DataOutputStream(so.getOutputStream());
    }
    catch (Exception e){
      e.printStackTrace();
    }

  }

  public void sendRegisterRequest(String username, String passwd)
  {

    String msgin = null;
    String msg = String.format("{\"header\": {\"request\": 1,}, \"creds\": {\"username\": \"%s\", \"passwd\": \"%s\",}}", username, passwd) ;

    System.out.printf("Sending register request with username %s and passwd %s", username, passwd);
    try{
    this.output.writeBytes(msg+'\n');
    }catch (Exception e)
    {
      e.printStackTrace();
    }

    try{
      while(msgin == null)
      {
        msgin = this.input.readLine();
        System.out.println("Response : "+ msgin);
        HandlerJsonResponse responseObj = new HandlerJsonResponse(msgin);
        System.out.println("1");
        responseObj.DetectResponseCode();
        System.out.println("2");
        responseObj.interpretResponse();
        System.out.println("3");
      }
    }catch (Exception e){
      e.printStackTrace();
    }

  }


}
