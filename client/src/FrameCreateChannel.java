package OnlineChatClient;

import OnlineChatServer.Utilities;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

public class FrameCreateChannel extends JFrame implements ActionListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private JPanel mainContainer;

    private JLabel channelIdLabel, channelNameLabel, channelPasswdLabel, message;

    private JTextField channelId, channelName, channelPasswd;

    private JButton create;

    public FrameCreateChannel() {
        super("Create a channel");

        channelIdLabel = new JLabel("Channel ID : ");
        channelId = new JTextField(6);
        channelNameLabel = new JLabel("Channel name : ");
        channelName = new JTextField(20);
        channelPasswdLabel = new JLabel("Channel password : ");
        channelPasswd = new JTextField(20);
        message = new JLabel("");
        create = new JButton("Create");
        

        //mainContainer
        mainContainer = new JPanel();
        mainContainer.add(channelIdLabel);
        mainContainer.add(channelId);
        mainContainer.add(channelNameLabel);
        mainContainer.add(channelName);
        mainContainer.add(channelPasswdLabel);
        mainContainer.add(channelPasswd);
        mainContainer.add(create);
        create.addActionListener(this);

        setContentPane(mainContainer);
		setLocationRelativeTo(null);//center the frame on screen
        setVisible(true);//set frame visible
        pack();
    }

    public void actionPerformed(ActionEvent e){

        Object src = e.getSource();
     
        if(src == create){

            String id = channelId.getText();
            String name = channelName.getText();
            String passwd = channelPasswd.getText();

            //check unique channel id
            System.out.println(Utilities.checkIfExist("../../server/src/databases/channels.txt", id));

            Utilities.addChannel(id);
            Utilities.updateChannelsDir();
            Channel chanel = new Channel(id, name, passwd);

        }
        

    }

}