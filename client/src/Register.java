package OnlineChatClient;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.regex.*;

public class Register extends JFrame implements DocumentListener, ActionListener, KeyListener{

    private JPanel mainFrame;
    private JPanel header;
    private JPanel body;
    private JPanel footer;
    private JButton register_bp;
    private JButton cancel_bp;


    private JPanel username_area;
    private JPanel passwd_area;
    private JLabel title;
    private JLabel username;
    private JLabel passwd;
    private JLabel passwdConfirm;
    private JTextField username_field;
    private JPasswordField passwd_field;
    private JPasswordField passwdConfirm_field;
    private JLabel error_username;
    private JLabel error_passwd;
    private JLabel error_passwdConfirm;
    private JLabel logo_register;

    boolean username_format = false;
    boolean passwd_format = false;
    boolean passwdConfirmCheck = false;

    public Register(){
      setTitle("Register");
      setMinimumSize(new Dimension(470, 470));
      setMaximumSize(new Dimension(520, 520));
      setResizable(false);
      mainFrame = new JPanel();
      setContentPane(mainFrame);
      mainFrame.setLayout(new GridBagLayout());
      GridBagConstraints gbc = new GridBagConstraints();


      header = new JPanel();
      body = new JPanel();
      footer = new JPanel();

      logo_register = new JLabel(new ImageIcon("../src/resources/img/tag.png"));
      username_area =new JPanel();
      username = new JLabel("Username : ");
      username_field = new JTextField(20);
      error_username = new JLabel("");
      passwd_area = new JPanel();
      passwd = new JLabel("Password : ");
      passwd_field = new JPasswordField(20);
      error_passwd = new JLabel("");
      passwdConfirm = new JLabel("Confirm password :");
      passwdConfirm_field = new JPasswordField(20);
      error_passwdConfirm = new JLabel("");
      register_bp = new JButton("Submit registration");
      cancel_bp = new JButton("Cancel registration");

      gbc.insets = new Insets(10, 15, 20, 15);
      gbc.gridx = 0;
      gbc.gridy = 0;
      mainFrame.add(header, gbc);

      gbc.gridx = 0;
      gbc.gridy = 1;
      body.setLayout(new GridBagLayout());
      mainFrame.add(body, gbc);

      gbc.gridx = 0;
      gbc.gridy = 2;
      footer.setLayout(new GridBagLayout());
      mainFrame.add(footer, gbc);

      header.add(logo_register);
      gbc.insets = new Insets(0, 0, 10, 0);
      gbc.gridx = 0;
      gbc.gridy = 0;
      username_area.setLayout(new GridBagLayout());
      body.add(username_area, gbc);

      gbc.insets = new Insets(10, 0, 0, 0);
      gbc.gridx = 0;
      gbc.gridy = 1;
      passwd_area.setLayout(new GridBagLayout());
      body.add(passwd_area, gbc);

      gbc.insets = new Insets(0, 0, 0, 0);
      gbc.gridx = 0;
      gbc.gridy = 0;
      username_area.add(username, gbc);
      gbc.gridx = 0;
      gbc.gridy = 1;
      username_area.add(username_field, gbc);
      username_field.getDocument().addDocumentListener(this);
      gbc.insets = new Insets(5, 0, 0, 0);
      gbc.gridx = 0;
      gbc.gridy = 2;
      username_area.add(error_username, gbc);
      error_username.setForeground(Color.red);


      gbc.gridx = 0;
      gbc.gridy = 0;
      passwd_area.add(passwd, gbc);
      gbc.gridx = 0;
      gbc.gridy = 1;
      passwd_area.add(passwd_field, gbc);
      passwd_field.addKeyListener(this);
      gbc.insets = new Insets(5, 0, 0, 0);
      gbc.gridx = 0;
      gbc.gridy = 2;
      passwd_area.add(error_passwd, gbc);
      error_passwd.setForeground(Color.red);
      gbc.gridx = 0;
      gbc.gridy = 3;
      passwd_area.add(passwdConfirm, gbc);
      gbc.gridx = 0;
      gbc.gridy = 4;
      passwd_area.add(passwdConfirm_field, gbc);
      passwdConfirm_field.addKeyListener(this);
      gbc.insets = new Insets(5, 0, 0, 0);
      gbc.gridx = 0;
      gbc.gridy = 5;
      passwd_area.add(error_passwdConfirm, gbc);
      error_passwdConfirm.setForeground(Color.red);


      gbc.insets = new Insets(0, 20, 0, 0);
      gbc.gridx = 0;
      gbc.gridy = 0;
      footer.add(register_bp, gbc);


      gbc.insets = new Insets(0, 0, 0, 20);
      gbc.gridx = 1;
      gbc.gridy = 0;
      footer.add(cancel_bp, gbc);
      cancel_bp.addActionListener(this);
      register_bp.addActionListener(this);
      register_bp.setEnabled(false);
      setLocationRelativeTo(null);
      setVisible(true);
      pack();
    }

    //Field events

    public void insertUpdate(DocumentEvent e)
    {
      CheckUsernameField();
    }

    public void removeUpdate(DocumentEvent e)
    {
      CheckUsernameField();
    }

    public void changedUpdate(DocumentEvent e){}

    public void trySetRegisterEnabled()
    {
      if(username_format & passwd_format & passwdConfirmCheck)
        register_bp.setEnabled(true);
      else
        register_bp.setEnabled(false);
    }

    private void CheckUsernameField()
    {
      if(!checkUserNameFormat(username_field.getText()))
      {
        if(username_field.getText().length() < 4){
          username_format = false;
          trySetRegisterEnabled();
          error_username.setText("Please use an username\nlonger than 4 caraters");
        }
        else if (username_field.getText().length() > 20){
          username_format = false;
          trySetRegisterEnabled();
          error_username.setText("Please use an username\nshorter than 20 caractres");
        }
        else{
          username_format = false;
          trySetRegisterEnabled();
          error_username.setText("Please do not\nuse special caracter");
        }
      }
      else{
        username_format = true;
        trySetRegisterEnabled();
        error_username.setText("");
      }
    }

    private void CheckPasswdField()
    {
      if(!checkPasswdFormat(new String(passwd_field.getPassword())))
      {
        passwd_format = false;
        trySetRegisterEnabled();
        error_passwd.setText("Use a password longer than 10 caracter and printables caracters");
      }
      else{
        passwd_format = true;
        trySetRegisterEnabled();
        error_passwd.setText("");
      }


      if(new String(passwd_field.getPassword()).equals(new String(passwdConfirm_field.getPassword()))) {
    	passwdConfirmCheck = true;
    	error_passwdConfirm.setText("");
    	trySetRegisterEnabled();
      } else {
    	  passwdConfirmCheck = false;
    	  trySetRegisterEnabled();
    	  error_passwdConfirm.setText("Passwords don't match");
      }

    }

    private boolean checkUserNameFormat(String username)
    {
      Matcher matcher;
      Pattern pattern_username;
      pattern_username = Pattern.compile("^(([a-zA-Z])|(\\d)|(-)|(_)){4,20}$");
      matcher = pattern_username.matcher(username);
      return matcher.find();
    }

    private boolean checkPasswdFormat(String passwd)
    {
      Matcher matcher;
      Pattern pattern_passwd;
      pattern_passwd = Pattern.compile("^(([a-zA-Z]|([\\_\\&\"\'\\(\\-\\)\\=\\<\\>\\,\\;\\:\\{\\}\\!\\.\\?\\+\\*\\$\\^\\/\\%\\]\\[\\|\\#\\~])|\\d){10,})$");
      matcher = pattern_passwd.matcher(passwd);
      return matcher.find();
    }

    //button action events
    public void actionPerformed(ActionEvent e){
      if(e.getSource() == cancel_bp)
        dispose();

      if(e.getSource() == register_bp)
        OnlineChatClient.Launch.rh.sendRegisterRequest(username_field.getText(), new String(passwd_field.getPassword()));

    }

    public void keyTyped(KeyEvent e)
    {
      CheckPasswdField();
    }

    public void keyReleased(KeyEvent e)
    {
      CheckPasswdField();
    }

    public void keyPressed(KeyEvent e)
    {
      CheckPasswdField();
    }
}
