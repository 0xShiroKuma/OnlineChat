package OnlineChatClient;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class AuthPanel extends JFrame implements ActionListener {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private JLabel logo;
	private JLabel mainPhrase;
	private JLabel message;
	private JLabel usernameLabel;
	private JLabel passwordLabel;

	private JTextField username;

	private JPasswordField password;

	JButton connect;
	private JButton register;
	private JButton exit;

	public AuthPanel() {

		super("Authentication panel");
		setSize(500, 500);

		logo = new JLabel(new ImageIcon("../src/resources/img/chat.png"));
		mainPhrase = new JLabel("OnlineChat");
		message = new JLabel("");
		usernameLabel = new JLabel("Username : ");
		passwordLabel = new JLabel("Password : ");
		username = new JTextField(15);
		password = new JPasswordField(15);
		connect = new JButton("Connect");
		register = new JButton("Register account");
		exit = new JButton("Exit");

		//Constraints
		GridBagConstraints c = new GridBagConstraints();

		JPanel mainContainer = new JPanel(); //main container
		mainContainer.setLayout(new GridBagLayout());

		//top panel
		JPanel topPanel = new JPanel();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = .25;
		topPanel.add(logo);
		topPanel.add(mainPhrase);
		topPanel.add(message);
		mainContainer.add(topPanel);

		//middle panel
		JPanel midPanel = new JPanel();
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 1;
		c.weighty = .25;
		JPanel usernamePanel = new JPanel();
			usernamePanel.add(usernameLabel);
			usernamePanel.add(username);
		midPanel.add(usernamePanel);
		JPanel passwordPanel = new JPanel();
			passwordPanel.add(passwordLabel);
			passwordPanel.add(password);
		midPanel.add(passwordPanel);
		mainContainer.add(midPanel);

		//bottom panel
		JPanel botPanel = new JPanel();
		c.gridx = 0;
		c.gridy = 2;
		c.weightx = 1;
		c.weighty = .25;
		connect.setPreferredSize(new Dimension(150, 50));
		botPanel.add(connect);
		connect.addActionListener(this);
		register.setPreferredSize(new Dimension(200, 50));
		botPanel.add(register);
		register.addActionListener(this);
		exit.setPreferredSize(new Dimension(150, 50));
		botPanel.add(exit);
		exit.addActionListener(this);
		mainContainer.add(botPanel);

		setContentPane(mainContainer);
		setLocationRelativeTo(null);//center the frame on screen
        setVisible(true);//set frame visible
        this.setLayout(new GridLayout(3, 1));

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//close the thread when frame is closed
	}



	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();

		if(src == exit) {
			this.dispose();
		}

		if(src == connect) {
			this.dispose();
			new Chat();
		}

		if(src == register) {
			new Register();
		}

	}

}
