package OnlineChatServer;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.InputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.BufferedReader;
import java.io.IOException;

class ClientHandler extends Thread{
    protected Socket soclient;
    protected int error = 0;
    ClientHandler(Socket soclient)
    {
      this.soclient= soclient;
    }

    public void run()
    {
      InputStream ins = null;
      DataOutputStream outs = null;
      BufferedReader br = null;

      try{
        ins = soclient.getInputStream();
        br = new BufferedReader(new InputStreamReader(ins));
        outs = new DataOutputStream(soclient.getOutputStream());
      }

      catch (IOException e)
      {
        System.out.println("Error during client connexion");
      }

      String msgin;
      String msgout;

      while(true)
      {
        try{
          msgin = br.readLine();
          if (msgin == null)
            continue;
          else{
            HandlerJsonRequest msgObj = new HandlerJsonRequest(msgin);
            error = msgObj.DetectRequestCode();
            if( error != 0){
              System.out.printf("Error %d\n",error);
              outs.writeUTF(Errors_responses.error(error));
            }
            else{
              error = msgObj.performRequest();
              if(error != 0){
                System.out.printf("Error %d\n",error);
                outs.writeBytes(Errors_responses.error(error)+'\n');
              }
            }
          }
        }catch (IOException e) {
          msgin = "";
          e.printStackTrace();
          return;
        }
      }
    }
}
