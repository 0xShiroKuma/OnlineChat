package OnlineChatServer;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class AdminPanel extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6;
	
	
	public AdminPanel() {
		
		super("Administration Panel");
		
		setSize(700, 500);
		
		button1 = new JButton("1");
		button2 = new JButton("2");
		button3 = new JButton("3");
		button4 = new JButton("4");
		button5 = new JButton("5");
		button6 = new JButton("6");
		
		JPanel mainContainer = new JPanel();
		
		//
		JPanel pan1 = new JPanel();
		pan1.add(button1);
		mainContainer.add(pan1);
		
		//
		JPanel pan2 = new JPanel();
		pan2.add(button2);
		mainContainer.add(pan2);
		
		//
		JPanel pan3 = new JPanel();
		pan3.add(button3);
		mainContainer.add(pan3);
		
		//
		JPanel pan4 = new JPanel();
		pan4.add(button4);
		mainContainer.add(pan4);
		
		//
		JPanel pan5 = new JPanel();
		pan5.add(button5);
		mainContainer.add(pan5);
		
		//
		JPanel pan6 = new JPanel();
		pan6.add(button6);
		mainContainer.add(pan6);
		
		
		mainContainer.setLayout(new GridLayout(2, 3));
		setContentPane(mainContainer);
		setLocationRelativeTo(null);
		setVisible(true);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		Object src = e.getSource();
		
		//
		if(src == button1) {
			
		}

		//
		if(src == button2) {
			
		}

		//
		if(src == button3) {
			
		}

		//
		if(src == button4) {
			
		}

		//
		if(src == button5) {
			
		}

		//
		if(src == button6) {
			
		}
		
	}

}
