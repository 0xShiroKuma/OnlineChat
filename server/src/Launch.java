package OnlineChatServer;

public class Launch {

	public static void main(String[] args) {

		Utilities.updateChannelsDir();
		
		new Account("Superbriochette35", "CookieMonster1");
		new Account("165843", "Jean-Michel", "chasse1234");
		
		new Channel("01234");
		new Channel("12345", "Super channel", "ezPasswd");
		new Channel("01337", "Admin channel", "POSEKFOISEFOINSROVIN");

		new MultiClientHandler().start();
	}

}
