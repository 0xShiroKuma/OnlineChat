package OnlineChatServer;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

class MultiClientHandler extends Thread{
  static final int port = 2300;

  MultiClientHandler()
  {
    System.out.println("[+] Start clients handler thread");
  }

  public void run()
  {
  System.out.println("[+] Clients handler started");
  ServerSocket serverso = null;
  Socket so = null;

  try
    {
      serverso = new ServerSocket(port);
    } catch (IOException e) {
    e.printStackTrace();
    }

    while(true)
    {

      try
      {
        so  = serverso.accept();
      } catch (IOException e) {
          System.out.println("I/O error: " + e);
      }

      new ClientHandler(so).start();
      }
    }
}
