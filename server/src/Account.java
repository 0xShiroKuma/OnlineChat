package OnlineChatServer;

public class Account {
	
	public String id, username, passwd, creationDate;
	
	public Account(String id, String username, String passwd) {
		
		this.id = id;
		this.username = username;
		this.passwd = passwd;
		
		Utilities.addAccount(this);
	}
	
	public Account(String username, String passwd) {
		this.id = String.format("%05d", 1+(int)(Math.random()*((99999-1)+1)));
		this.username = username;
		this.passwd = passwd;
		
		Utilities.addAccount(this);
	}
	
	
	public String getId() {
		return this.id;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public String getPasswd() {
		return this.passwd;
	}
	
	
	public void sendMessage(Channel chan, String message) {
		
		
		
	}
	
}
