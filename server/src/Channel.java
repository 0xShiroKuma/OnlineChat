package OnlineChatServer;

import org.json.*;
import java.io.FileWriter;

public class Channel {
	
	public String id, name, passwd, creationDate, path;
	
	public Channel(String id, String name, String passwd) {
		
		this.id = id;
		this.name = name;
		this.passwd = passwd;
		this.creationDate = Utilities.getCurrentDate();
		
		Utilities.addChannel(this.id);
		
		writeJsonFiles(this);
		
	}
	
	public Channel(String id) {
		
		this.id = id;
		this.name = "Unnamed channel";
		this.passwd = "";
		this.creationDate = Utilities.getCurrentDate();
		
		Utilities.addChannel(this.id);
		
		writeJsonFiles(this);
		
	}
	
	
	
	public String getId(){
		return this.id;
	}

	public String getName(){
		return this.name;
	}

	public String getPasswd(){
		return this.passwd;
	}
	
	
	
	
	
	
	
	private void writeJsonFiles(Channel chan) {
		
		path = "../src/databases/channels/" + chan.id + "/";
		
		//fill the json files
		try {
			//create the JSonObjects
			JSONObject channel = new JSONObject();
			channel.put("passwd", chan.passwd);
			channel.put("users", chan.getUsers());
		
			JSONObject channel_desc = new JSONObject();
			channel_desc.put("name", chan.name);
			channel_desc.put("creation-date", chan.creationDate);
			
			JSONObject channel_msg = new JSONObject();
			channel_msg.put("message", new JSONObject()
				.put("senderId", "id")
				.put("senderName", "user")
				.put("content", "message content")
				.put("date", "yyyy/MM/dd - HH:mm:ss"));
			
			//write them into files
			FileWriter fwChannel = new FileWriter(path + "channel.json");
			fwChannel.write(channel.toString());
			fwChannel.close();
		
			FileWriter fwChannelDesc = new FileWriter(path + "channel.desc.json");
			fwChannelDesc.write(channel_desc.toString());
			fwChannelDesc.close();
		
			FileWriter fwChannelMsg = new FileWriter(path + "channel.msg.json");
			fwChannelMsg.write(channel_msg.toString());
			fwChannelMsg.close();
		
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
}

