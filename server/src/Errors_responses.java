package OnlineChatServer;

class Errors_responses{
  protected static String error( int r)
  {
    switch (r) {
      case 1:
      return String.format("{\"header\": {\"response_code\": 2,}, \"error\": {\"error_code\": %d, \"error_description\": \"%s\",}}", r, "bad request header code") ;

      case 2:
      return String.format("{\"header\": {\"response_code\": 2,}, \"error\": {\"error_code\": %d, \"error_description\": \"%s\",}}", r, "request header code doesn't exist") ;

      case 3:
      return String.format("{\"header\": {\"response_code\": 2,}, \"error\": {\"error_code\": %d, \"error_description\": \"%s\",}}", r, "bad username format") ;

      case 4:
      return String.format("{\"header\": {\"response_code\": 2,}, \"error\": {\"error_code\": %d, \"error_description\": \"%s\",}}", r, "bad passwd format") ;

      case 5:
      return String.format("{\"header\": {\"response_code\": 2,}, \"error\": {\"error_code\": %d, \"error_description\": \"%s\",}}", r, "username exits") ;

      default: return String.format("{\"header\": {\"response_code\": 2,}, \"error\": {\"error_code\": %d, \"error_description\": \"%s\",}}", r, "unknown error");
    }
  }
}


//{"header": {"response_code": 2,}, "error": {"error_code": 5, "error_description": "username exits",}}
