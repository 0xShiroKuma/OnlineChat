package OnlineChatServer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.*;

public class Utilities {

	private static String usersFile = "../src/databases/users.txt";
	private static String channelsFile = "../src/databases/channels.txt";
	private static String channelsDir = "../src/databases/channels/";
	
	
	public static void updateChannelsDir() {
		
		File dir = new File(channelsDir);
		File[] dirList = dir.listFiles();
		ArrayList<String> channels = listAllChannels();
		
		//
		if(dirList != null) {
			
			//we create one folder per channel, plus json files in it
			for(String channel : channels) {
				
				try {
					new File(dir + "/" + channel + "/").mkdirs();
					new File(dir + "/" + channel + "/channel.json").createNewFile();
					new File(dir + "/" + channel + "/channel.desc.json").createNewFile();
					new File(dir + "/" + channel + "/channel.msg.json").createNewFile();
					new File(dir + "/" + channel + "/channel.users.txt").createNewFile();
					System.out.println("Channel " + channel + " has been updated");
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
			
		}
		
	}
	
	
	
	public static ArrayList<String> listAllAccounts(){
		
		ArrayList<String> list = new ArrayList<String>();
		Account acc;
		
		try {
			
			FileReader fr = new FileReader(usersFile);
			BufferedReader br = new BufferedReader(fr);
			String line;
			
			while((line = br.readLine()) != null) {
				list.add(line);
			}
			
			br.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	
	public String getAccountByIndex(int index) {
		
		String line = null;
		
		try {
			
			FileReader fr = new FileReader(usersFile);
			BufferedReader br = new BufferedReader(fr);
			int i=0;
			while(i<index) {
				line = br.readLine();
				i++;
			}
			
			br.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return line;
	}
	
	
	public String getAccountById(String id) {
		
		String line = null;
		
		try {
			
			FileReader fr = new FileReader(usersFile);
			BufferedReader br = new BufferedReader(fr);

			while((line = br.readLine()) != null) {
				if(line.substring(0, 5) == id)
					break;
			}
			
			br.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return line;
	}
	
	
	
	public static void addAccount(Account acc) {
		
		Boolean accExist = false;
		ArrayList<String> accList = listAllAccounts();
		
		try {
			
			for(int i=0; i<accList.size(); i++) {
				if( acc.getUsername().equals( parseAccount(accList.get(i))[1] ) ) {
					accExist = true;
				}
			}
			
			if(!accExist) {
				FileWriter fw = new FileWriter(usersFile, true);
				BufferedWriter bw = new BufferedWriter(fw);

				bw.write(acc.getId() + ':' + acc.getUsername() + ':' + acc.getPasswd() + '\n');
			
				bw.close();
				System.out.println("Added account with id " + acc.getId());
			} else {
				System.out.println("Account already exist");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static String[] parseAccount(String account) {
		String[] acc = account.split(":");
		return acc;
	}
	
	
	
	public static boolean checkIfExist(String file, String str) {
		
		boolean exist = false;
		
		try {
			
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				if(line.equals(str))
					exist = true;
			}
			
			br.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return exist;
	}
	
	
	public static void addChannel(String id) {
		
		try {
			if(!checkIfExist(channelsFile, id)) {
				FileWriter fw = new FileWriter(channelsFile, true);
				BufferedWriter bw = new BufferedWriter(fw);
			
				bw.write(id + '\n');
			
				bw.close();
				System.out.println("Added channel " + id);
			
				updateChannelsDir();
			} else
				System.out.println("Channel already exist");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static void addChannel() {
		
		String id;
		int rand = 1+(int)(Math.random()*((99999-1)+1));
		
		
		while(checkIfExist(channelsFile, String.format("%05d", rand))) {
			System.out.println("Channel with id " + String.format("%05d", rand) + " already exist, trying a other id...");
			rand = 1+(int)(Math.random()*((99999-1)+1));
		}
		
		id = String.format("%05d", rand);
		
		addChannel(id);
		
	}
	
	public static ArrayList<String> listAllChannels() {
		ArrayList<String> channels = new ArrayList<String>();
		
		try {
			
			FileReader fr = new FileReader(channelsFile);
			BufferedReader br = new BufferedReader(fr);
			String line = null;
			
			while((line = br.readLine()) != null) {
				channels.add(line);
			}
			
			br.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return channels;
	}
	
	
	
	public static Channel getChannelById(String id) {
		Channel channel = null;

		try {

			FileReader fr = new FileReader(channelsFile);
			BufferedReader br = new BufferedReader(fr);
			String line = null;
			
			while((line = br.readLine()) != null) {
				
				if(line.equals(id)){

					File channelDesc = new File(channelsDir + "/" + id + "/channel.desc.json");
					File channelFile = new File(channelsDir + "/" + id + "/channel.json");
					channelId = id;
					channelName = channelDesc.readLine().split(",")[0].split(":")[1];
					channelPasswd = channelFile.readLine().split(":")[1];

					channel = new Channel(channelId, channelName, channelPasswd);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return channel;

	}
	
	
	
	public static String getCurrentDate() {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm:ss");
		
		return dateFormat.format(date);
	}
	
	
	
	public static ArrayList<Channel> getAccountConnectedChannels(String accountId) {

		ArrayList<Channel> channels = {};

		String[] allChannels = listAllChannels();

		//waiting to know where store channels' users
		
		for(int i=0;i<allChannels.length;i++){

			

		}

		return channels;
	}
	
	
	
	public static boolean connectToChannel(Account account, Channel channel, String passwd){

		boolean connected = false;

		channelId = channel.getId();
		channelPasswd = channel.getPasswd();

		if(channelPasswd.equals(passwd)){
			addUserToChannel(account, channel);
			connected = true;
		}

		return connected;

	}
	
	
	
	public static void addUserToChannel(Account acc, Channel channel){

		String accountId = acc.getId();

		String channelId = channel.getId();

		try {

			File usersFile = new File(channelsDir + "/" + channelId + "/channel.users.txt");
			FileWriter fw = new FileWritter(usersFile, true);
			BufferedWritter bw = new BufferedWritter(fw);

			bw.write(accountId + '\n');

			bw.close();

		} catch(Exception e){
			e.printStackTrace();
		}

	}
	
	
	
	public static void postMessage(Account acc, Channel channel, String message){

		String accountId = acc.getId();
		String accountName = acc.getName();
		String date = getCurrentDate();

		String channelId = channel.getId();
		
		try {
			File msgFile = new File(channelsDir + "/" + channelId + "/channel.msg.json");

			JSONObject channel_msg = new JSONObject();
			channel_msg.put("message", new JSONObject()
				.put("senderId", accoundId)
				.put("senderName", accountName)
				.put("content", message)
				.put("date", date));

			FileWriter fwChannelMsg = new FileWriter(msgFile, true);
			fwChannelMsg.write(channel_msg.toString());
			fwChannelMsg.close();

		} catch(Exception e){
			e.printStackTrace();
		}

	}
	
	
	
	public static String[] getChannelMessages(Channel channel){

		String[] messages = {};

		try {

			File msgFile = new File(channelsDir + "/" + channelId + "/channel.msg.json");
			FileReader fr = new FileReader(msgFile);
			BufferedReader br = new BufferedReader(fr);

			String line, mess = "";
			String[] array = {}, content = {}, arr = {};

			while((line = br.readLine() != null)){
				array = line.split("{" + '"' + "message" + '"' + ":{");
			}

			for(int i=0;i<array.length;i++){
				content = array[i].split(',');
				arr[0] = content[0].split(':')[1];
				arr[1] = content[1].split(':')[1];
				arr[2] = content[2].split(':')[1];
				arr[3] = content[3].split(':')[1];
				//get only the content for the moment
				mess = arr[2];
				messages.add(mess);
			}

			br.close();

		} catch (Exception e){
			e.printStackTrace();
		}

		return messages;

	}
	

	
}
